from rest_framework import serializers

from .models import Item, Tag, User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username',)


class TagSerializer(serializers.ModelSerializer):
    display = serializers.SerializerMethodField()

    class Meta:
        model = Tag
        fields = '__all__'

    def get_display(self, obj):
        return f'{obj.id}. {obj.name}'


class ItemSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)

    class Meta:
        model = Item
        exclude = ('user',)

