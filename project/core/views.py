from django.utils.timezone import now
from rest_framework import viewsets, mixins
from rest_framework.generics import GenericAPIView, CreateAPIView
from rest_framework.authentication import (
    SessionAuthentication,
    TokenAuthentication
)
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from rest_framework.response import Response

from .models import Item, Tag, User
from .filters import TagFilter, ItemFilter
from .serializers import ItemSerializer, TagSerializer, UserSerializer


class RegisterUser(CreateAPIView):
    # TODO:
    # 1. Валидация - сериализатор (username, сложный пароль) - использовал djoser
    # 2. Создать пользователя User.objects.create_user() - использовал djoser, url: /auth/users/
    # 3. Создать токен - токен создаётся и отдаётся при логине
    # 4. Отдать токен - токен создаётся и отдаётся при логине
    queryset = User.objects.all()
    serializer_class = UserSerializer


class LoginUser(GenericAPIView):
    # TODO:
    # 1. Валидация - сериализатор (username, пароль)
    # 2. Отдать токен
    pass
    # использовал obtain_auth_token (в urls.py)


class ItemViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated, DjangoModelPermissions)
    serializer_class = ItemSerializer
    filterset_class = ItemFilter

    def get_queryset(self):
        return Item.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(detail=True, methods=['POST', 'PUT', 'PATCH'])
    def set_done(self, request, pk=None):
        Item.objects.filter(pk=pk, done__isnull=True).update(done=now())
        return Response({'message': 'ok'})

    @action(detail=True, methods=['POST', 'PUT', 'PATCH'])
    def unset_done(self, request, pk=None):
        item: Item = self.get_object()
        if item.done:
            item.done = None
            item.save(update_fields=['done'])
        serializer = ItemSerializer(instance=item)
        return Response(serializer.data)


class TagViewSet(viewsets.ReadOnlyModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    queryset = Tag.objects.all()
    permission_classes = (IsAuthenticated, DjangoModelPermissions)
    serializer_class = TagSerializer
    filterset_class = TagFilter
