from django.contrib import admin

from .models import Tag, Item


@admin.register(Tag)
class Tag(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)


@admin.register(Item)
class Item(admin.ModelAdmin):
    list_display = ('name', 'done',)
    search_fields = ('name',)
