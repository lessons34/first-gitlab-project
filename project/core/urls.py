from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import ItemViewSet, TagViewSet

router_v1 = DefaultRouter()
router_v1.register('items', ItemViewSet, basename='item')
router_v1.register('tags', TagViewSet, basename='tag')


urlpatterns = [
    path('', include(router_v1.urls)),
]
