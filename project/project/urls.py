from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='API')

urlpatterns = [
    path('', schema_view),
    path('', include('core.urls')),
    path('auth/', include('users.urls')),
    path('admin/', admin.site.urls),
]
