from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token

from core.views import RegisterUser, LoginUser

urlpatterns = [
    path('', include('djoser.urls')),
    path('token/', obtain_auth_token),
]